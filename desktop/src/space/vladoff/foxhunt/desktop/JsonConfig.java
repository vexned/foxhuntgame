package space.vladoff.foxhunt.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import java.awt.Dimension;
import java.awt.Toolkit;

public class JsonConfig {
    private transient LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
    private int width;
    private int height;
    private boolean fullscreen;
    private boolean useGL30;
    private boolean vsync;
    private float soundVolume;

    public JsonConfig() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.width = (int)screenSize.getWidth();
        this.height = (int)screenSize.getHeight();
        this.config.title = "FoxHunting";
        this.config.resizable = false;
        this.config.forceExit = true;
        this.config.useHDPI = false;
        this.soundVolume = 1.0F;
        this.fullscreen = true;
        this.useGL30 = false;
        this.vsync = true;
    }

    public void apply() {
        this.config.width = this.width;
        this.config.height = this.height;
        this.config.fullscreen = this.fullscreen;
        this.config.useGL30 = this.useGL30;
        this.config.vSyncEnabled = this.vsync;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isFullscreen() {
        return this.fullscreen;
    }

    public void setFullscreen(boolean fullscreen) {
        this.fullscreen = fullscreen;
    }

    public boolean isUseGL30() {
        return this.useGL30;
    }

    public void setUseGL30(boolean useGL30) {
        this.useGL30 = useGL30;
    }

    public boolean isVsync() {
        return this.vsync;
    }

    public void setVsync(boolean vsync) {
        this.vsync = vsync;
    }

    public void setSoundVolume(float soundVolume) {
        this.soundVolume = soundVolume;
    }

    public LwjglApplicationConfiguration getConfig() {
        return this.config;
    }

    public float getSoundVolume() {
        return this.soundVolume;
    }
}
