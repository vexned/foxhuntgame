package space.vladoff.foxhunt.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import space.vladoff.foxhunt.desktop.JsonConfig;
import com.badlogic.gdx.utils.Json;
import space.vladoff.foxhunt.FoxHuntGame;
import space.vladoff.foxhunt.screens.MainApplication;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;

public class DesktopLauncher {
	public static void main (String[] arg) throws IOException {
		Json json = new Json();
		JsonConfig jsonConfig = new JsonConfig();

		try {
			Reader configReader = new FileReader(System.getProperty("user.dir") + "/configPC.json");
			jsonConfig = (JsonConfig)json.fromJson(JsonConfig.class, configReader);
			jsonConfig.apply();
			configReader.close();
		} catch (Exception var7) {
			try {
				FileWriter writer = new FileWriter(System.getProperty("user.dir") + "/configPC.json");
				jsonConfig = new JsonConfig();
				writer.append(json.prettyPrint(jsonConfig));
				writer.close();
				jsonConfig.apply();
			} catch (IOException var6) {
				var6.printStackTrace();
			}
		}

		new LwjglApplication(new MainApplication(jsonConfig.getSoundVolume()), jsonConfig.getConfig());
	}
}
