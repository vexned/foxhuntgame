package space.vladoff.foxhunt;

public class Game {
    private Field gameField;
    private int points;

    public Game(int x, int y) {
        this.gameField = new Field(x, y);
        this.points = x * y - this.gameField.getFoxCount();
    }

    public Field getField() {
        return this.gameField;
    }

    public int getPoints() {
        return this.points;
    }

    public String getPointsString() {
        return Integer.toString(this.points);
    }

    public boolean check(int x, int y) {
        if (this.gameField.getCells()[x][y].getState()) {
            return false;
        } else {
            this.gameField.getCells()[x][y].setState(true);
            if (this.gameField.getCells()[x][y].isFox()) {
                this.gameField.foxFound();
                return true;
            } else {

                --this.points;
                int count = 0;
                for(Fox fox: gameField.getFoxes()) {
                    count += fox.isIntersect(x, y) ? 1 : 0;
                }



//                Fox fox;
//                for(Iterator var4 = this.gameField.getFoxes().iterator(); var4.hasNext(); ) {
//                    fox = (Fox)var4.next();
//                }

                this.gameField.getCells()[x][y].setNumber(count);
                return false;
            }
        }
    }

    public boolean isGameFinished() {
        return this.getField().getFoxCount() == 0;
    }
}
