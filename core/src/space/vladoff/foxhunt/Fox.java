package space.vladoff.foxhunt;

public class Fox {
    private int xcoord;
    private int ycoord;

    public Fox(int x, int y) {
        this.xcoord = x;
        this.ycoord = y;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o != null && this.getClass() == o.getClass()) {
            Fox fox = (Fox)o;
            if (this.xcoord != fox.xcoord) {
                return false;
            } else {
                return this.ycoord == fox.ycoord;
            }
        } else {
            return false;
        }
    }

    public int hashCode() {
        int result = this.xcoord;
        result = 31 * result + this.ycoord;
        return result;
    }

    public int getX() {
        return this.xcoord;
    }

    public int getY() {
        return this.ycoord;
    }

    public boolean isIntersect(int x, int y) {
        return this.xcoord == x || this.ycoord == y || this.ycoord - this.xcoord == y - x || this.ycoord + this.xcoord == y + x;
    }
}
