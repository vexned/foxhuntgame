package space.vladoff.foxhunt;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import java.util.Iterator;
import space.vladoff.foxhunt.util.Block;
import space.vladoff.foxhunt.util.DebugInformationView;

public class GenericGame extends ApplicationAdapter {
    private OrthographicCamera camera;
    private Viewport viewport;
    private Viewport backgroundViewport;
    private SpriteBatch batch;
    private Game game;
    private TextureAtlas contents;
    private TextureRegion fox;
    private TextureRegion dirt;
    private TextureRegion grass;
    private Texture background;
    private Array<AtlasRegion> breakAnimationRegions;
    private Animation breakAnimation;
    private Sound foxYay;
    private Array<Block> cells;
    private Vector3 touchPos;
    private ObjectMap<Integer, TextureRegion> numberMap;
    private float backgroungHeight;
    private float backgroundWidth;
    private int positionShift;
    private DebugInformationView debugView;
    private static final float FRAME_DURATION = 0.008333334F;
    private static final float MIN_SCENE_WIDTH = 700.0F;
    private static final float MIN_SCENE_HEIGHT = 1244.0F;
    private static final float MAX_SCENE_WIDTH = 1400.0F;
    private static final float MAX_SCENE_HEIGHT = 1244.0F;
    private static final float CELL_RENDER_SIZE = 64.0F;
    private int tx;
    private int ty;

    public GenericGame() {
    }

    public void create() {
        this.batch = new SpriteBatch();
        this.camera = new OrthographicCamera();
        this.viewport = new ExtendViewport(700.0F, 1244.0F, this.camera);
        this.contents = new TextureAtlas(Gdx.files.internal("Contents.pack"));
        this.fox = this.contents.findRegion("fox");
        this.dirt = this.contents.findRegion("dirt");
        this.grass = this.contents.findRegion("GrassBlock");
        this.breakAnimationRegions = new Array();

        int i;
        AtlasRegion temp;
        for(i = 0; (temp = this.contents.findRegion("GrassBreakAnim" + i)) != null; ++i) {
            this.breakAnimationRegions.add(temp);
        }

        this.numberMap = new ObjectMap();
        this.breakAnimation = new Animation(0.008333334F, this.breakAnimationRegions, PlayMode.NORMAL);

        for(i = 0; (temp = this.contents.findRegion("N" + i)) != null; ++i) {
            this.numberMap.put(i, temp);
        }

        this.background = new Texture(Gdx.files.internal("Background.png"));
        this.touchPos = new Vector3();
        this.foxYay = Gdx.audio.newSound(Gdx.files.internal("got.mp3"));
        int partyCount = 8;
        this.positionShift = partyCount * 64 / 2;
        this.game = new Game(partyCount, partyCount);
        this.cells = new Array();

        for(i = 0; i < partyCount; ++i) {
            for(int j = 0; j < partyCount; ++j) {
                this.cells.add(new Block(this.grass, this.breakAnimation, (float)(i * 64 - this.positionShift), (float)(j * 64 - this.positionShift), 64.0F, 64.0F));
            }
        }

        this.debugView = new DebugInformationView(this.viewport);
    }

    public void resize(int width, int height) {
        this.viewport.update(width, height);
    }

    public void render() {
        Gdx.gl.glClearColor(0.0F, 0.0F, 0.2F, 1.0F);
        Gdx.gl.glClear(16384);
        this.camera.update();
        this.batch.setProjectionMatrix(this.camera.combined);
        this.batch.begin();
        this.backgroundWidth = (float)this.background.getWidth() * (this.viewport.getWorldHeight() / (float)this.background.getHeight());
        this.backgroungHeight = this.viewport.getWorldHeight();
        Iterator var1 = this.cells.iterator();

        Block cell;
        while(var1.hasNext()) {
            cell = (Block)var1.next();
            this.tx = (int)((cell.getX() + (float)this.positionShift) / 64.0F);
            this.ty = (int)((cell.getY() + (float)this.positionShift) / 64.0F);
            if (this.game.getField().getCells()[this.tx][this.ty].getState()) {
                if (this.game.getField().getCells()[this.tx][this.ty].isFox()) {
                    this.batch.draw(this.fox, cell.getX(), cell.getY());
                    cell.playBreakAnimation(this.batch);
                } else {
                    cell.draw(this.batch);
                    cell.playBreakAnimation(this.batch);
                }
            } else {
                cell.draw(this.batch);
            }
        }

        this.batch.end();
        if (Gdx.input.justTouched()) {
            this.touchPos.set((float)Gdx.input.getX(), (float)Gdx.input.getY(), 0.0F);
            this.camera.unproject(this.touchPos);
            var1 = this.cells.iterator();

            while(var1.hasNext()) {
                cell = (Block)var1.next();
                if (cell.rectangle.contains(this.touchPos.x, this.touchPos.y)) {
                    cell.setTexture(this.dirt);
                    if (this.game.check((int)(cell.getX() + (float)this.positionShift) / 64, (int)(cell.getY() + (float)this.positionShift) / 64)) {
                        this.foxYay.play();
                    } else {
                        cell.setNumber((TextureRegion)this.numberMap.get(this.game.getField().getCells()[(int)(cell.getX() + (float)this.positionShift) / 64][(int)(cell.getY() + (float)this.positionShift) / 64].getNumber()));
                    }
                    break;
                }
            }
        }

        this.debugView.show(this.batch);
    }

    public void dispose() {
        this.batch.dispose();
        this.contents.dispose();
        this.background.dispose();
        this.foxYay.dispose();
        this.debugView.dispose();
    }
}
