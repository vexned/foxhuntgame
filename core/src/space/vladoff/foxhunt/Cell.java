package space.vladoff.foxhunt;

public class Cell {
    private boolean state = false;
    private boolean isFox = false;
    private int number = 0;

    Cell() {
    }

    public void setFox(boolean val) {
        this.isFox = true;
    }

    public void setState(boolean val) {
        this.state = val;
    }

    public void setNumber(int val) {
        this.number = val;
    }

    public boolean isFox() {
        return this.isFox;
    }

    public boolean getState() {
        return this.state;
    }

    public int getNumber() {
        return this.number;
    }
}
