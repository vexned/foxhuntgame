//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package space.vladoff.foxhunt.util;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class UITest extends ApplicationAdapter {
    Object[] listEntries = new Object[]{"This is a list entry1", "And another one1", "The meaning of life1", "Is hard to come by1", "This is a list entry2", "And another one2", "The meaning of life2", "Is hard to come by2", "This is a list entry3", "And another one3", "The meaning of life3", "Is hard to come by3", "This is a list entry4", "And another one4", "The meaning of life4", "Is hard to come by4", "This is a list entry5", "And another one5", "The meaning of life5", "Is hard to come by5"};
    Skin skin;
    Stage stage;
    Texture texture1;
    Texture texture2;
    Label fpsLabel;

    public UITest() {
    }

    public void create() {
        this.skin = new Skin(Gdx.files.internal("uiskin.json"));
        this.texture1 = new Texture(Gdx.files.internal("data/badlogicsmall.jpg"));
        this.texture2 = new Texture(Gdx.files.internal("data/badlogic.jpg"));
        TextureRegion image = new TextureRegion(this.texture1);
        TextureRegion imageFlipped = new TextureRegion(image);
        imageFlipped.flip(true, true);
        TextureRegion image2 = new TextureRegion(this.texture2);
        this.stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(this.stage);
        ImageButtonStyle style = new ImageButtonStyle((ButtonStyle)this.skin.get(ButtonStyle.class));
        style.imageUp = new TextureRegionDrawable(image);
        style.imageDown = new TextureRegionDrawable(imageFlipped);
        ImageButton iconButton = new ImageButton(style);
        Button buttonMulti = new TextButton("Multi\nLine\nToggle", this.skin, "toggle");
        Button imgButton = new Button(new Image(image), this.skin);
        Button imgToggleButton = new Button(new Image(image), this.skin, "toggle");
        Label myLabel = new Label("this is some text.", this.skin);
        myLabel.setWrap(true);
        Table t = new Table();
        t.row();
        t.add(myLabel);
        t.debug();
        t.layout();
        final CheckBox checkBox = new CheckBox(" Continuous rendering", this.skin);
        checkBox.setChecked(true);
        final Slider slider = new Slider(0.0F, 10.0F, 1.0F, false, this.skin);
        slider.setAnimateDuration(0.3F);
        TextField textfield = new TextField("", this.skin);
        textfield.setMessageText("Click here!");
        textfield.setAlignment(1);
        final SelectBox selectBox = new SelectBox(this.skin);
        selectBox.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                System.out.println(selectBox.getSelected());
            }
        });
        selectBox.setItems(new Object[]{"Android1", "Windows1 long text in item", "Linux1", "OSX1", "Android2", "Windows2", "Linux2", "OSX2", "Android3", "Windows3", "Linux3", "OSX3", "Android4", "Windows4", "Linux4", "OSX4", "Android5", "Windows5", "Linux5", "OSX5", "Android6", "Windows6", "Linux6", "OSX6", "Android7", "Windows7", "Linux7", "OSX7"});
        selectBox.setSelected("Linux6");
        Image imageActor = new Image(image2);
        ScrollPane scrollPane = new ScrollPane(imageActor);
        List list = new List(this.skin);
        list.setItems(this.listEntries);
        list.getSelection().setMultiple(true);
        list.getSelection().setRequired(false);
        ScrollPane scrollPane2 = new ScrollPane(list, this.skin);
        scrollPane2.setFlickScroll(false);
        SplitPane splitPane = new SplitPane(scrollPane, scrollPane2, false, this.skin, "default-horizontal");
        this.fpsLabel = new Label("fps:", this.skin);
        Label passwordLabel = new Label("Textfield in password mode: ", this.skin);
        TextField passwordTextField = new TextField("", this.skin);
        passwordTextField.setMessageText("password");
        passwordTextField.setPasswordCharacter('*');
        passwordTextField.setPasswordMode(true);
        Table tooltipTable = new Table(this.skin);
        tooltipTable.pad(10.0F).background("default-round");
        tooltipTable.add(new TextButton("Fancy tooltip!", this.skin));
        Window window = new Window("Dialog", this.skin);
        window.getTitleTable().add(new TextButton("X", this.skin)).height(window.getPadTop());
        window.setPosition(0.0F, 0.0F);
        window.defaults().spaceBottom(10.0F);
        window.row().fill().expandX();
        window.add(iconButton);
        window.add(buttonMulti);
        window.add(imgButton);
        window.add(imgToggleButton);
        window.row();
        window.add(checkBox);
        window.add(slider).minWidth(100.0F).fillX().colspan(3);
        window.row();
        window.add(selectBox).maxWidth(100.0F);
        window.add(textfield).minWidth(100.0F).expandX().fillX().colspan(3);
        window.row();
        window.add(splitPane).fill().expand().colspan(4).maxHeight(200.0F);
        window.row();
        window.add(passwordLabel).colspan(2);
        window.add(passwordTextField).minWidth(100.0F).expandX().fillX().colspan(2);
        window.row();
        window.add(this.fpsLabel).colspan(4);
        window.pack();
        this.stage.addActor(window);
        textfield.setTextFieldListener(new TextFieldListener() {
            public void keyTyped(TextField textField, char key) {
                if (key == '\n') {
                    textField.getOnscreenKeyboard().show(false);
                }

            }
        });
        slider.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.log("UITest", "slider: " + slider.getValue());
            }
        });
        iconButton.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                (new Dialog("Some Dialog", UITest.this.skin, "dialog") {
                    protected void result(Object object) {
                        System.out.println("Chosen: " + object);
                    }
                }).text("Are you enjoying this demo?").button("Yes", true).button("No", false).key(66, true).key(131, false).show(UITest.this.stage);
            }
        });
        checkBox.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.graphics.setContinuousRendering(checkBox.isChecked());
            }
        });
    }

    public void render() {
        Gdx.gl.glClearColor(0.2F, 0.2F, 0.2F, 1.0F);
        Gdx.gl.glClear(16384);
        this.fpsLabel.setText("fps: " + Gdx.graphics.getFramesPerSecond());
        this.stage.act(Math.min(Gdx.graphics.getDeltaTime(), 0.033333335F));
        this.stage.draw();
    }

    public void resize(int width, int height) {
        this.stage.getViewport().update(width, height, true);
    }

    public void dispose() {
        this.stage.dispose();
        this.skin.dispose();
        this.texture1.dispose();
        this.texture2.dispose();
    }
}
