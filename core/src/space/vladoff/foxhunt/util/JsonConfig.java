
package space.vladoff.foxhunt.util;

public class JsonConfig {
    private int width = 0;
    private int height = 0;
    private boolean fullscreen = true;
    private boolean useGL30 = false;
    private boolean vsync = true;
    private float soundVolume = 1.0F;

    public JsonConfig() {
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isFullscreen() {
        return this.fullscreen;
    }

    public void setFullscreen(boolean fullscreen) {
        this.fullscreen = fullscreen;
    }

    public boolean isUseGL30() {
        return this.useGL30;
    }

    public void setUseGL30(boolean useGL30) {
        this.useGL30 = useGL30;
    }

    public boolean isVsync() {
        return this.vsync;
    }

    public void setVsync(boolean vsync) {
        this.vsync = vsync;
    }

    public void setSoundVolume(float soundVolume) {
        this.soundVolume = soundVolume;
    }

    public float getSoundVolume() {
        return this.soundVolume;
    }
}
