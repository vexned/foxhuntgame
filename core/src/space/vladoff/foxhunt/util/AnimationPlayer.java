package space.vladoff.foxhunt.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimationPlayer {
    private float time;
    private Batch batch;
    private Animation animation;

    public AnimationPlayer(Batch batch, Animation animation) {
        this.batch = batch;
        this.animation = animation;
    }

    public void play(float x, float y, float width, float height) {
        this.time += Gdx.graphics.getDeltaTime();
        this.batch.draw((TextureRegion)this.animation.getKeyFrame(this.time), x, y, width, height);
    }
}
