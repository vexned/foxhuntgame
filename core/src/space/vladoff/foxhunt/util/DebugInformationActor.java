package space.vladoff.foxhunt.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.profiling.GLProfiler;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.StringBuilder;

public class DebugInformationActor extends Actor {
    private Label label;
    private Skin skin;
    private Stage stage;
    private StringBuilder sb;
    private GLProfiler profiler;

    public DebugInformationActor(Stage stage, Skin skin) {
        this.skin = skin;
        this.stage = stage;
        this.label = new Label("", skin);
        profiler = new GLProfiler(Gdx.graphics);
        profiler.enable();
        this.sb = new StringBuilder();
        this.label.setFillParent(true);
    }

    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        this.label.setPosition(20.0F, this.getTop() - 80.0F);
        this.sb.append("Кадров в секунду: ").append(Gdx.graphics.getFramesPerSecond()).append("\nВызовов отрисовки: ").append(profiler.getDrawCalls()).append("\nВызовов: ").append(profiler.getCalls()).append("\nВывсота экрана:").append(this.stage.getViewport().getScreenHeight()).append("\nШирина экрана: ").append(this.stage.getViewport().getScreenWidth()).append("\nВысота мира: ").append(this.stage.getViewport().getWorldHeight()).append("\nШирина мира: ").append(this.stage.getViewport().getWorldWidth());
        this.label.setText(this.sb.toString());
        this.label.draw(batch, parentAlpha);
        profiler.reset();
        this.sb.setLength(0);
    }

    public void act(float delta) {
        super.act(delta);
    }
}
