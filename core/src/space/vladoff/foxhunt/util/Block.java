package space.vladoff.foxhunt.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class Block {
    TextureRegion texture;
    TextureRegion number;
    Animation breakAnimation;
    float animationTime;
    public Rectangle rectangle;
    boolean isOpen;
    float fontx;
    float fonty;

    public Block(TextureRegion region, Animation animation, float x, float y, float width, float height) {
        this.texture = region;
        this.breakAnimation = animation;
        this.rectangle = new Rectangle(x, y, width, height);
        this.fontx = x + width * 0.5F - 17.0F;
        this.fonty = y + height * 0.5F - 15.0F;
        this.isOpen = false;
    }

    public void playBreakAnimation(Batch batch) {
        if (!this.breakAnimation.isAnimationFinished(this.animationTime)) {
            this.animationTime += Gdx.graphics.getDeltaTime();
            batch.draw((TextureRegion)this.breakAnimation.getKeyFrame(this.animationTime), this.rectangle.x, this.rectangle.y, this.rectangle.width, this.rectangle.height);
        }
    }

    public void setTexture(TextureRegion region) {
        this.texture = region;
    }

    public void setNumber(TextureRegion number) {
        this.number = number;
        this.isOpen = true;
    }

    public void draw(Batch batch) {
        batch.draw(this.texture, this.rectangle.x, this.rectangle.y, this.rectangle.width, this.rectangle.height);
        if (this.isOpen) {
            batch.draw(this.number, this.fontx, this.fonty);
        }

    }

    public float getX() {
        return this.rectangle.x;
    }

    public float getY() {
        return this.rectangle.y;
    }
}
