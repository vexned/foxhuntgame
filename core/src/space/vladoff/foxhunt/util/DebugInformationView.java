
package space.vladoff.foxhunt.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.profiling.GLProfiler;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.StringBuilder;
import com.badlogic.gdx.utils.viewport.Viewport;

public class DebugInformationView implements Disposable {
    private BitmapFont font;
    private Viewport viewport;
    private StringBuilder sb;
    private Texture fontTexture;
    private GLProfiler profiler;

    public DebugInformationView(Viewport viewport) {
        this.fontTexture = new Texture(Gdx.files.internal("FontStd.png"), true);
        this.fontTexture.setFilter(TextureFilter.MipMap, TextureFilter.MipMap);
        this.font = new BitmapFont(Gdx.files.internal("cheeseusaceu-32.fnt"), new TextureRegion(this.fontTexture));
        this.viewport = viewport;
        profiler = new GLProfiler(Gdx.graphics);
        profiler.enable();
        this.sb = new StringBuilder();

    }

    public void show(Batch batch) {
        float w = this.viewport.getWorldWidth() * 0.5F;
        float h = this.viewport.getWorldHeight() * 0.5F;
        this.sb.append("Кадров в секунду: ").append(Gdx.graphics.getFramesPerSecond()).append("\nВызовов отрисовки: ").append(profiler.getDrawCalls()).append("\nВызовов: ").append(profiler.getCalls()).append("\nВывсота экрана:").append(this.viewport.getScreenHeight()).append("\nШирина экрана: ").append(this.viewport.getScreenWidth()).append("\nВысота мира: ").append(this.viewport.getWorldHeight()).append("\nШирина мира: ").append(this.viewport.getWorldWidth());
        batch.begin();
        this.font.draw(batch, this.sb.toString(), -w + 20.0F, h - 20.0F);
        batch.end();
        profiler.reset();
        this.sb.setLength(0);
    }

    public void dispose() {
        this.fontTexture.dispose();
    }
}
