package space.vladoff.foxhunt;

import com.badlogic.gdx.math.MathUtils;
import java.util.HashSet;
import java.util.Iterator;

public class Field {
    private int sizex;
    private int sizey;
    private int foxCount;
    private Cell[][] cells;
    private HashSet<Fox> foxes;

    public Field(int x, int y) {
        this.sizex = x;
        this.sizey = y;
        this.foxCount = (int)((double)(x * y) / 12.5D);
        this.cells = new Cell[x][y];

        for(int i = 0; i < x; ++i) {
            for(int j = 0; j < y; ++j) {
                this.cells[i][j] = new Cell();
            }
        }

        this.foxes = new HashSet();

        do {
            this.foxes.add(new Fox(MathUtils.random(0, x - 1), MathUtils.random(0, y - 1)));
        } while(this.foxes.size() < this.foxCount);

        Iterator var5 = this.foxes.iterator();

        while(var5.hasNext()) {
            Fox fox = (Fox)var5.next();
            this.cells[fox.getX()][fox.getY()].setFox(true);
        }

    }

    public int getSizeX() {
        return this.sizex;
    }

    public int getSizeY() {
        return this.sizey;
    }

    public int getFoxCount() {
        return this.foxCount;
    }

    public void foxFound() {
        --this.foxCount;
    }

    public Cell[][] getCells() {
        return this.cells;
    }

    public HashSet<Fox> getFoxes() {
        return this.foxes;
    }
}