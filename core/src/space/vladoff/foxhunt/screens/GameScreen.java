package space.vladoff.foxhunt.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import space.vladoff.foxhunt.Game;
import space.vladoff.foxhunt.util.Block;

public class GameScreen implements Screen {
    private final AssetManager assetManager;
    private final MainApplication application;
    private OrthographicCamera camera;
    private Viewport viewport;
    private SpriteBatch batch;
    private Game game;
    private TextureAtlas contents;
    private TextureRegion fox;
    private TextureRegion dirt;
    private TextureRegion grass;
    private Texture background;
    private Array<AtlasRegion> breakAnimationRegions;
    private Animation breakAnimation;
    private Sound foxYaySound;
    private Array<Block> cells;
    private Vector3 touchPosition;
    private ObjectMap<Integer, TextureRegion> numberMap;
    private Texture fieldBackground;
    private BitmapFont font;
    private Texture gameOver;
    private Texture OKTexture;
    private Rectangle OKBounds;
    private float backgroungHeight;
    private float backgroundWidth;
    private float positionShift;
    private float cellSize;
    private static final float FRAME_DURATION = 1/120;
    private static final float MIN_SCENE_WIDTH = 700;
    private static final float MIN_SCENE_HEIGHT = 1244;
    private static final float MAX_SCENE_WIDTH = 1400;
    private static final float MAX_SCENE_HEIGHT = 1244;
    private static final float CELL_RENDER_SIZE = 64;
    private int tx;
    private int ty;

    public GameScreen(AssetManager manager, MainApplication application, int partyCount, Skin skin) {
        this.assetManager = manager;
        this.application = application;

        this.batch = new SpriteBatch();
        this.camera = new OrthographicCamera();

        this.viewport = new ExtendViewport(MIN_SCENE_WIDTH, MIN_SCENE_HEIGHT, this.camera);
        this.font = skin.getFont("font_troika_30pt_russian");

        this.contents = this.assetManager.get("Contents.pack", TextureAtlas.class);
        this.fieldBackground = this.assetManager.get("fieldBackground.png", Texture.class);
        this.fox = this.contents.findRegion("fox");
        this.dirt = this.contents.findRegion("dirt");
        this.grass = this.contents.findRegion("GrassBlock");
        this.gameOver = this.assetManager.get("over.png", Texture.class);
        this.OKTexture = this.assetManager.get("OK.png", Texture.class);
        this.OKBounds = new Rectangle(-12.0F, -120.0F, (float)this.OKTexture.getWidth(), (float)this.OKTexture.getHeight());
        this.breakAnimationRegions = new Array();

        AtlasRegion temp;
        for(int i = 0; (temp = this.contents.findRegion("GrassBreakAnim" + i)) != null; ++i)
            this.breakAnimationRegions.add(temp);

        this.numberMap = new ObjectMap();
        this.breakAnimation = new Animation(FRAME_DURATION, this.breakAnimationRegions, PlayMode.NORMAL);

        for(int i = 0; (temp = this.contents.findRegion("N" + i)) != null; ++i)
            this.numberMap.put(i, temp);

        this.background = this.assetManager.get("Background.png", Texture.class);
        this.touchPosition = new Vector3();
        this.foxYaySound = this.assetManager.get("got.mp3", Sound.class);

        loadAssets();

        this.cellSize = (float)(640 / partyCount);
        this.positionShift = (float)partyCount * this.cellSize * 0.5f;
        this.game = new Game(partyCount, partyCount);
        this.cells = new Array();

        for(int i = 0; i < partyCount; ++i) {
            for(int j = 0; j < partyCount; ++j) {
                this.cells.add(new Block(this.grass, this.breakAnimation,
                        (float)i * this.cellSize - this.positionShift,
                        (float)j * this.cellSize - this.positionShift,
                        this.cellSize, this.cellSize));
            }
        }
    }

    public void loadAssets() {

    }

    public void show() {
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.2f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_CLEAR_VALUE);

        this.camera.update();
        this.batch.setProjectionMatrix(this.camera.combined);

        this.backgroundWidth = (float)this.background.getWidth() * (this.viewport.getWorldHeight() / (float)this.background.getHeight());
        this.backgroungHeight = this.viewport.getWorldHeight();

        this.batch.begin();
        this.batch.draw(this.background,
                -this.backgroundWidth * 0.5F,
                -this.backgroungHeight * 0.5F,
                    this.backgroundWidth,
                    this.backgroungHeight);

        if (this.game.isGameFinished()) {

            this.batch.draw(this.gameOver,
                    (float)(-this.gameOver.getWidth()) * 0.5F,
                    (float)(-this.gameOver.getHeight()) * 0.5F);

            this.font.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
            this.font.getData().setScale(2.0f);
            this.font.draw(this.batch, this.game.getPointsString(), 0.0F, 0.0F);

            this.batch.draw(this.OKTexture, -25.0f, -120.0f);

            if (Gdx.input.justTouched()) {
                this.touchPosition.set((float)Gdx.input.getX(), (float)Gdx.input.getY(), 0.0F);
                this.camera.unproject(this.touchPosition);
                if (this.OKBounds.contains(this.touchPosition.x, this.touchPosition.y)) {
                    application.setScreen(new MainMenuScreen(this.application));
                }
            }

            this.batch.end();

        } else {

            if (Gdx.input.justTouched()) {
                this.touchPosition.set((float)Gdx.input.getX(), (float)Gdx.input.getY(), 0.0F);
                this.camera.unproject(this.touchPosition);

                for(Block cell: cells) {
                    if (cell.rectangle.contains(this.touchPosition.x, this.touchPosition.y)) {
                        cell.setTexture(this.dirt);
                        if (game.check((int)((cell.getX() + this.positionShift) / this.cellSize), (int)((cell.getY() + this.positionShift) / this.cellSize))) {
                            foxYaySound.play();
                        } else {
                            cell.setNumber(numberMap.get(game.getField().getCells()[(int)((cell.getX() + this.positionShift) / this.cellSize)][(int)((cell.getY() + this.positionShift) / this.cellSize)].getNumber()));
                        }
                        break;
                    }
                }
            }

            this.batch.draw(this.fieldBackground, (float)(-this.fieldBackground.getWidth()) * 0.5F, (float)(-this.fieldBackground.getHeight()) * 0.5F + 30.0F);

            for(Block cell: cells) {
                this.tx = (int)((cell.getX() + this.positionShift) / this.cellSize);
                this.ty = (int)((cell.getY() + this.positionShift) / this.cellSize);
                if (this.game.getField().getCells()[this.tx][this.ty].getState()) {
                    if (this.game.getField().getCells()[this.tx][this.ty].isFox()) {
                        this.batch.draw(this.fox, cell.getX(), cell.getY(), this.cellSize, this.cellSize);
                        cell.playBreakAnimation(this.batch);
                    } else {
                        cell.draw(this.batch);
                        cell.playBreakAnimation(this.batch);
                    }
                } else {
                    cell.draw(this.batch);
                }
            }

            this.font.draw(this.batch, "Очки: " + this.game.getPoints(), (float)(-this.fieldBackground.getWidth()) * 0.5F + 20.0F, (float)this.fieldBackground.getHeight() * 0.5F);
            this.batch.end();
        }

    }

    public void resize(int width, int height) {
        this.viewport.update(width, height);
    }

    public void pause() {
    }

    public void resume() {
    }

    public void hide() {
    }

    public void dispose() {
        this.fieldBackground.dispose();
        this.font.dispose();
        this.batch.dispose();
        this.contents.dispose();
        this.background.dispose();
        this.foxYaySound.dispose();
    }
}
