package space.vladoff.foxhunt.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.StringBuilder;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import space.vladoff.foxhunt.util.JsonConfig;
import space.vladoff.foxhunt.util.DebugInformationActor;

import java.io.*;

public class MainMenuScreen implements Screen {
    final MainApplication mainApplication;
    private OrthographicCamera camera;
    private Viewport viewport;
    private Skin skin;
    private Stage stage;
    private DebugInformationActor debugInfo;
    private Image bck;
    private Image logo;
    private Table menu;
    private InputProcessor defaultProcessor;
    private AssetManager manager;
    private float soundVolume;

    public MainMenuScreen(MainApplication mainApplication) {
        this.mainApplication = mainApplication;
        this.manager = new AssetManager();
        this.loadAssets();
        this.camera = new OrthographicCamera(720.0F, 1280.0F);
        this.viewport = new ScreenViewport(this.camera);
        this.skin = new Skin(Gdx.files.internal("uiskin.json"));
        this.stage = new Stage(this.viewport);
        this.defaultProcessor = Gdx.input.getInputProcessor();
        Gdx.input.setInputProcessor(this.stage);
        this.bck = new Image(new TextureRegion((Texture)this.manager.get("Background.png")));
        this.logo = new Image(new TextureRegion((Texture)this.manager.get("logo.png", Texture.class)));
        Container logoContainer = new Container(this.logo);
        logoContainer.center();
        logoContainer.setFillParent(true);
        logoContainer.center().top();
        this.stage.addActor(this.bck);
        this.menu = this.createMainMenu();
        this.menu.setFillParent(true);
        Table mainTable = new Table();
        mainTable.setFillParent(true);
        mainTable.left().bottom();
        mainTable.add(logoContainer).width(logoContainer.getMaxWidth()).height(logoContainer.getMaxHeight());
        mainTable.row();
        mainTable.add(this.menu).width(this.menu.getMaxWidth()).height(this.menu.getMaxHeight());
        this.stage.addActor(mainTable);
    }

    public void loadAssets() {
        this.manager.load("Background.png", Texture.class);
        this.manager.load("logo.png", Texture.class);
        this.manager.load("Easy.png", Texture.class);
        this.manager.load("Medium.png", Texture.class);
        this.manager.load("Hard.png", Texture.class);
        this.manager.load("Contents.pack", TextureAtlas.class);
        this.manager.load("got.mp3", Sound.class);
        this.manager.load("fieldBackground.png", Texture.class);
        this.manager.load("over.png", Texture.class);
        this.manager.load("OK.png", Texture.class);
        this.manager.finishLoading();
    }

    public Table createMainMenu() {
        Button newGame = new TextButton("Новая игра", this.skin, "Large");
        Button training = new TextButton("Обучение", this.skin, "Large");
        Button settings = new TextButton("Настройки", this.skin, "Large");
        Button exit = new TextButton("Выход", this.skin, "Large");
        newGame.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                Table lol = MainMenuScreen.this.createDifficultyMenu();
                lol.setFillParent(true);
                MainMenuScreen.this.menu.setVisible(false);
                MainMenuScreen.this.stage.addActor(lol);
            }
        });
        exit.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
            }
        });
        settings.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                Window window = MainMenuScreen.this.createSettingsWindow();
                window.center();
                MainMenuScreen.this.menu.setVisible(false);
                MainMenuScreen.this.stage.addActor(window);
            }
        });
        training.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                Window window = MainMenuScreen.this.createTrainingWindow();
                window.center();
                MainMenuScreen.this.menu.setVisible(false);
                MainMenuScreen.this.stage.addActor(window);
            }
        });
        Table table = new Table();
        float globalHeight = this.viewport.getWorldHeight() * 0.1F;
        float globalWidth = this.viewport.getWorldWidth() * 0.8F;
        table.center().bottom();
        table.add(newGame).pad(0.0F, 0.0F, 5.0F, 0.0F).width(globalWidth).height(globalHeight);
        table.row();
        table.add(training).pad(5.0F, 0.0F, 5.0F, 0.0F).width(globalWidth).height(globalHeight);
        table.row();
        table.add(settings).pad(5.0F, 0.0F, 5.0F, 0.0F).width(globalWidth).height(globalHeight);
        table.row();
        table.add(exit).pad(5.0F, 0.0F, 0.0F, 0.0F).width(globalWidth).height(globalHeight);
        table.row();
        table.add().width(globalWidth).height(globalHeight);
        return table;
    }

    public Table createDifficultyMenu() {
        Button Easy = new ImageButton(new TextureRegionDrawable(new TextureRegion((Texture)this.manager.get("Easy.png", Texture.class))));
        Button Medium = new ImageButton(new TextureRegionDrawable(new TextureRegion((Texture)this.manager.get("Medium.png", Texture.class))));
        Button Hard = new ImageButton(new TextureRegionDrawable(new TextureRegion((Texture)this.manager.get("Hard.png", Texture.class))));
        Easy.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.input.setInputProcessor(MainMenuScreen.this.defaultProcessor);
                MainMenuScreen.this.mainApplication.setScreen(new GameScreen(MainMenuScreen.this.manager, MainMenuScreen.this.mainApplication, 6, MainMenuScreen.this.skin));
            }
        });
        Medium.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.input.setInputProcessor(MainMenuScreen.this.defaultProcessor);
                MainMenuScreen.this.mainApplication.setScreen(new GameScreen(MainMenuScreen.this.manager, MainMenuScreen.this.mainApplication, 8, MainMenuScreen.this.skin));
            }
        });
        Hard.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.input.setInputProcessor(MainMenuScreen.this.defaultProcessor);
                MainMenuScreen.this.mainApplication.setScreen(new GameScreen(MainMenuScreen.this.manager, MainMenuScreen.this.mainApplication, 10, MainMenuScreen.this.skin));
            }
        });
        Label EasyTip = new Label("Расчитан на небольшие игровые сессии", this.skin);
        Label MediumTip = new Label("Вам придется хорошо подумать!", this.skin);
        Label HardTip = new Label("Режим для профессионалов. Расчитан на долгую игровую сессию!", this.skin);
        Table table = new Table();
        table.center().bottom();
        float globalHeight = this.viewport.getWorldHeight() * 0.2F;
        float globalWidth = this.viewport.getWorldWidth() * 0.8F;
        table.add(Easy).height(globalHeight);
        table.row();
        table.add(EasyTip);
        table.row();
        table.add(Medium).height(globalHeight);
        table.row();
        table.add(MediumTip);
        table.row();
        table.add(Hard).height(globalHeight);
        table.row();
        table.add(HardTip);
        table.row();
        table.add().height(40.0F);
        return table;
    }

    public Window createTrainingWindow() {
        String text = "1. На поле случайным неизвестным для игрока образом расставляются «лисы».\n2. Игрок задает свое положение отмечая клетку на экране. В ответ он получает количество «лис», которое пеленгуется из его нынешнего местоположения. Это число указывает, сколько лис расположено в одной вертикали, горизонтали и диагоналях с указанной клеткой.\n3. Если местоположение игрока совпало с положением «лисы», она считается найденной.\n4. Игра продолжается, пока не будут найдены все «лисы».\n";
        Label label = new Label(text, this.skin);
        Button ok = new TextButton("OK", this.skin);
        label.setWrap(true);
        final Window window = new Window("Обучение", this.skin);
        ok.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                window.remove();
                MainMenuScreen.this.menu.setVisible(true);
            }
        });
        window.center();
        window.defaults().spaceBottom(10.0F).pad(10.0F, 10.0F, 10.0F, 10.0F);
        window.add(label).width(this.viewport.getWorldWidth() * 0.7F).left();
        window.row();
        window.add(ok).center();
        window.setSize(label.getWidth(), label.getHeight());
        window.pack();
        window.setPosition(this.viewport.getWorldWidth() * 0.5F - window.getWidth() * 0.5F, this.viewport.getWorldHeight() * 0.5F - window.getHeight() * 0.5F);
        return window;
    }

    public Window createSettingsWindow() {
        JsonConfig readedJsonConfig = null;
        final Json json = new Json();

        try {
            Reader configReader = new FileReader(System.getProperty("user.dir") + "/configPC.json");
            readedJsonConfig = (JsonConfig)json.fromJson(JsonConfig.class, configReader);
            configReader.close();
        } catch (FileNotFoundException var15) {
            if(readedJsonConfig == null) {
                readedJsonConfig = new JsonConfig();
            }
        } catch (IOException var16) {
            var16.printStackTrace();
        }

        final JsonConfig jsonConfig = readedJsonConfig;

        Label resolutionText = new Label("Разрешение", this.skin);
        Label loudnessText = new Label("Громкость", this.skin);
        final Slider loudness = new Slider(0.0F, 1.0F, 0.01F, false, this.skin);
        loudness.setValue(jsonConfig.getSoundVolume());
        final SelectBox selectBox = new SelectBox(this.skin);
        selectBox.setItems(new Object[]{"800x600", "1024x768", "1280x720", "1366x768", "1440x900", "1920x1080"});
        StringBuilder strb = new StringBuilder();
        strb.append(jsonConfig.getWidth()).append("x").append(jsonConfig.getHeight());
        selectBox.setSelected(strb.toString());
        final CheckBox fullscreen = new CheckBox("Полный экран", this.skin);
        fullscreen.setChecked(jsonConfig.isFullscreen());
        final CheckBox vsync = new CheckBox("V-Sync", this.skin);
        vsync.setChecked(jsonConfig.isVsync());
        final CheckBox GL30 = new CheckBox("OpenGL 3.0", this.skin);
        GL30.setChecked(jsonConfig.isUseGL30());
        Button ok = new TextButton("OK", this.skin);
        Button cancel = new TextButton("Отмена", this.skin);
        final Window window = new Window("Hactpouku", this.skin);
        ok.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                try {
                    JsonConfig local = jsonConfig;
                    local.setFullscreen(fullscreen.isChecked());
                    local.setSoundVolume(loudness.getValue());
                    local.setUseGL30(GL30.isChecked());
                    local.setVsync(vsync.isChecked());
                    String[] resString = ((String)selectBox.getSelected()).split("x");
                    int width = Integer.parseInt(resString[0]);
                    int height = Integer.parseInt(resString[1]);
                    local.setHeight(height);
                    local.setWidth(width);
                    FileWriter writer = new FileWriter(System.getProperty("user.dir") + "/configPC.json");
                    writer.append(json.prettyPrint(local));
                    writer.close();
                    final Dialog dlg = new Dialog("Нужен перезапуск!", MainMenuScreen.this.skin);
                    dlg.text("Нужно перезапустить игру,\n для того чтобы изменения вступили в силу!");
                    dlg.button("OK").addListener(new ChangeListener() {
                        public void changed(ChangeEvent event, Actor actor) {
                            dlg.hide();
                        }
                    });
                    dlg.defaults().spaceBottom(10.0F).pad(10.0F, 10.0F, 10.0F, 10.0F);
                    dlg.pack();
                    dlg.show(MainMenuScreen.this.stage);
                    window.remove();
                    MainMenuScreen.this.menu.setVisible(true);
                } catch (IOException ex) {
                    final Dialog dlgx = new Dialog("Error!", MainMenuScreen.this.skin);
                    dlgx.text("Error while writing settings!");
                    dlgx.button("  OK  ").addListener(new ChangeListener() {
                        public void changed(ChangeEvent event, Actor actor) {
                            dlgx.hide();
                        }
                    });
                    dlgx.defaults().spaceBottom(10.0F).pad(10.0F, 10.0F, 10.0F, 10.0F);
                    dlgx.pack();
                    dlgx.show(MainMenuScreen.this.stage);
                    window.remove();
                    MainMenuScreen.this.menu.setVisible(true);
                }

            }
        });
        cancel.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                window.remove();
                MainMenuScreen.this.menu.setVisible(true);
            }
        });
        window.center();
        window.defaults().spaceBottom(10.0F).pad(10.0F, 10.0F, 10.0F, 10.0F);
        window.row().fill().expandX();
        window.add(resolutionText);
        window.add(selectBox);
        window.row().fill().expandX();
        window.add(loudnessText);
        window.add(loudness);
        window.row();
        window.add(fullscreen).colspan(2).left();
        window.row();
        window.add(vsync).colspan(2).left();
        window.row();
        window.add(GL30).colspan(2).left();
        window.row().fill().expandX();
        window.add(ok);
        window.add(cancel);
        window.pack();
        window.setPosition(this.viewport.getWorldWidth() * 0.5F - window.getWidth() * 0.5F, this.viewport.getWorldHeight() * 0.5F - window.getHeight() * 0.5F);
        return window;
    }

    public void show() {
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(0.2F, 0.2F, 0.2F, 1.0F);
        Gdx.gl.glClear(16384);
        this.stage.act(Math.min(Gdx.graphics.getDeltaTime(), 0.033333335F));
        this.stage.draw();
    }

    public void resize(int width, int height) {
        this.stage.getViewport().update(width, height, true);
        this.bck.setSize(this.bck.getDrawable().getMinWidth() * ((float)height / this.bck.getDrawable().getMinHeight()), (float)height);
    }

    public void pause() {
    }

    public void resume() {
    }

    public void hide() {
    }

    public void dispose() {
        this.skin.dispose();
        this.stage.dispose();
    }
}