package space.vladoff.foxhunt.screens;

import com.badlogic.gdx.Game;

public class MainApplication extends Game {
    private float masterSound = 1.0F;

    public MainApplication(float sound) {
        this.masterSound = sound;
    }

    public void create() {
        this.setScreen(new MainMenuScreen(this));
    }

    public void render() {
        super.render();
    }

    public void dispose() {
    }
}